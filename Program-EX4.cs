﻿using System;
namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            // a Find the smallest element in the array

            int[] arr = { 300, 200, 100, 400 };
            TanHai cls = new TanHai();
            int SmallNumber = cls.GetSmallestNumber(arr);
            Console.WriteLine($"So nho nhat trong mang la: {SmallNumber}");

            // b Find the largest element in the array

            int LargestNumber = cls.GetLargestNumber(arr);
            Console.WriteLine($"So lon nhat trong mang la: {LargestNumber}");

            // c Move the smallest element to the top of the array
            
            int minIndex = cls.GetIndexSmallestNumber(arr);
            Console.WriteLine("Vi tri nho nhat {0} voi gia tri {1}", minIndex, arr[minIndex]);

            int topMinIndex;
            topMinIndex = arr[minIndex];
            arr[minIndex] = arr[0];
            arr[0] = topMinIndex;
                 
            // d Move the largest element to the top of the array

            int maxIndex = cls.GetIndexLargestNumber(arr);
            Console.WriteLine("Vi tri lon nhat {0} voi gia tri {1}", maxIndex, arr[maxIndex]);

            int topMaxIndex;
            topMaxIndex = arr[maxIndex];
            arr[maxIndex] = arr[0];
            arr[0] = topMaxIndex;

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            
            // e Move the smallest element to the last of the array
            
            int bottomMinIndex;
            bottomMinIndex = arr[minIndex];
            arr[minIndex] = arr[^1];
            arr[^1] = bottomMinIndex;

            // f Move the largest element to the last of the array

            int bottomMaxIndex;
            bottomMaxIndex = arr[maxIndex];
            arr[maxIndex] = arr[^1];
            arr[^1] = bottomMaxIndex;

            // g Sort the array using selection sort (ascending order)
            
            int selectionArray = cls.Selection(arr);
            int n_selection = arr.Length;
            for (int i = 0; i < n_selection; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            
            

            // h Sort the array using bubble sort (ascending order)

            int bubbleArray = cls.Bubble(arr);
            int n_bubble = arr.Length;
            for (int i = 0; i < n_bubble; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();

            // i Sort the array following descending order

            int descendingArray = cls.Descending(arr);
            int n_descending = arr.Length;
            for (int i = 0; i < n_descending; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();

        }
    }
}

