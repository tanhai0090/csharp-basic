﻿using System;
namespace EX4
{
    class TanHai
    {
        internal int GetSmallestNumber(int[] a)
        {
            int min = a[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < min)
                {
                    min = a[i];
                }
            }
            return min;
        }
        internal int GetLargestNumber(int[] a)
        {
            int max = a[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i]> max)
                {
                    max = a[i];
                }
            }
            return max;
        }

        internal int GetIndexSmallestNumber(int[] a)
        {
            int currentMinIndex = 0;
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] < a[currentMinIndex])
                {
                    currentMinIndex = i;
                }
            }
            return currentMinIndex;
        }
        internal int GetIndexLargestNumber(int[] a)
        {
            int currentMaxIndex = 0;
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] > a[currentMaxIndex])
                {
                    currentMaxIndex = i;
                }
            }
            return currentMaxIndex;
        }
        internal int Selection(int [] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min_index = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (arr[j] < arr[min_index])
                    {
                        min_index = j;
                    }
                }
                int temp = arr[min_index];
                arr[min_index] = arr[i];
                arr[i] = temp;
            }
            return n;  
         
        }
        internal int Bubble(int[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n ; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            return n;
        }
        internal int Descending(int[] arr)
        { 
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min_index = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (arr[j] > arr[min_index])
                    {
                        min_index = j;
                    }
                }
                int temp = arr[min_index];
                arr[min_index] = arr[i];
                arr[i] = temp;
            }
            return n;

        }

    }
}
