﻿using System;

namespace EX3
{
	class Program
	{
        public static string End { get; private set; }

		static void Main(string[] args)
		{
            
			//1
			Console.WriteLine("_______________¶______");
			Console.WriteLine("| Love OOP&C#  ||l\"\"|\"\"\\__,__");
			Console.WriteLine("|______________|||__|__|__|]");
			Console.WriteLine("(@)@)**********(@)(@)**(@)");
			//2
			int i;
			Console.Write("Enter a Number : ");
			i = int.Parse(Console.ReadLine());
			if (i % 2 == 0)
			{
				Console.WriteLine("Entered Number is an Even Number");
			}
			else
			{
				Console.WriteLine("Entered Number is an Odd Number");
			}
			//3
			Console.Write("Enter number 01: ");
			double number1 = double.Parse(Console.ReadLine());
			Console.Write("Enter number 02: ");
			double number2 = double.Parse(Console.ReadLine());
			Console.Write("Enter number 03: ");
			double number3 = double.Parse(Console.ReadLine());
			if (number1 > number2)
			{
				if (number1 > number3) 
				{
					Console.WriteLine($"{number1} is the largest number");
				}
				else
				{
					Console.WriteLine($"{number3} is the largest number");
				}
			}
			else if (number2 > number3)
			{
				Console.WriteLine($"{number2} is the largest number");
			}
			else
			{
				Console.WriteLine($"{number3} is the largest number");
			}

			if (number1 < number2)
			{
				if (number1 < number3)
				{
					Console.WriteLine($"{number1} is the smallest number");
				}
				else
				{
					Console.WriteLine($"{number3} is the smallest number");
				}
			}
			else if (number2 < number3)
			{
				Console.WriteLine($"{number2} is the smallest number");
			}
			else
			{
				Console.WriteLine($"{number3} is the smallest number");
			}

			Console.Write($"the average of {number1}, {number2}, {number3} numbers is: ");
			Console.WriteLine((number1 + number2 + number3) / 3);
			//4
			Console.Write("Enter grade: ");
			double d = double.Parse(Console.ReadLine());
			if (d <= 100 && d > 90)
			{


				Console.WriteLine("A");

			}
			else if (d <= 90 && d > 80)
			{

				Console.WriteLine("B");

			}
			else if (d <= 80 && d > 70)
			{

				Console.WriteLine("C");

			}
			else if (d <= 70 && d > 60)
			{

				Console.WriteLine("D");

			}
			else if (d <= 60 && d >= 0)
			{

				Console.WriteLine("F");

			}


			//5
			Console.Write("Enter Month: ");
			int Month = int.Parse(Console.ReadLine());
			Console.Write("Enter Year: ");
			int Year = int.Parse(Console.ReadLine());
            IsleafYear(Year);
			Console.Write($"the number days in month {Month} year {Year}  ");
			Console.WriteLine(" " + Dayinmontth(Month, Year)+ "Days");
            //6
			string day1, day2;
			Console.Write("Enter M:D:Y (ONE): ");
			day1 = Console.ReadLine();
			Console.Write("Enter M:D:Y (TWO): ");
			day2 = Console.ReadLine();
			DateTime date1 = Convert.ToDateTime(day1);
			DateTime date2 = Convert.ToDateTime(day2);
			int SoNgay;
			SoNgay = (date1 - date2).Days;
			Console.Write($"Between {date1} (ONE) - {date2} (TOW) = ");
			Console.Write(Math.Abs(SoNgay) + " Days");
            Console.WriteLine("");
			
            //7
            Console.Write("Enter N1: ");
			int n1 = int.Parse(Console.ReadLine());
            for (int x = 0; x < n1; x++)
            {
                Console.Write("*");
            }
            Console.WriteLine("");
			Console.Write("Enter N2: ");
			int n2 = int.Parse(Console.ReadLine());
			for (int z = 0; z < n2; z++)
			{
				Console.Write("* \n" );
			}
			Console.WriteLine("");
			
			Console.Write("Enter N3: ");
			int n3 = int.Parse(Console.ReadLine());
			for (int c = 0; c < n3; c++)
			{
                for (int j = 0; j <= c; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("\n");
			}
			

			//8

			Console.Write("Enter N4: ");
			int h = Convert.ToInt32(Console.ReadLine());

			if (h > 0)
			{
				for (int v = 1; v <= h; v++)
				{
					for (int j = 1; j < 2 * h; j++)
					{
						if (Math.Abs(h - j) == v - 1 || v == h)
						{
							Console.Write(" * ");
						}
						else
						{
							Console.Write("   ");
						}
					}
					Console.WriteLine("\n");
				}
			}
			else
			{
				Console.WriteLine("Enter N4 > 0");
			}

			


			Console.Write("Enter N5 : ");
			int H = Convert.ToInt32(Console.ReadLine());
			int a = (2 * H - 1);
			int b = (a / 2 + 1);
			for (int g = 1; g <= H; g++)
			{
				for (int j = 1; j <= a; j++)
				{
					if (g == 1)
					{
						if ((j % 2 != 0))
						{
							Console.Write("*");
						}
						else
						{
							Console.Write(" ");
						}
					}
					else if ((j == g) || (j == a + 1 - g))
					{
						Console.Write("*");
					}
					else
					{
						Console.Write(" ");
					}
					
				}
				Console.Write("\n");
            }
			Console.Write("\n");

			
            Console.Write("Enter N6: ");
			int Size = int.Parse(Console.ReadLine());
			Alphabet_N_Pattern(Size);




		}

		static bool IsleafYear(int year)
		{
			if (year % 4 == 0)
			{
				if (year % 400 == 0)
				{
					return true;
				}
				return false;
			}
			return false;
		}

		static int Dayinmontth(int month, int year)
		{
			int day = 0;
			switch (month)
			{
				case 1:
					day = 31;
					break;
				case 2:
					if (IsleafYear(year))
					{
						day = 29;
					}
					day = 28;
					break;
				case 3:
					day = 31;
					break;
				case 4:
					day = 30;
					break;
				case 5:
					day = 31;
					break;
				case 6:
					day = 30;
					break;
				case 7:
					day = 31;
					break;
				case 8:
					day = 31;
					break;
				case 9:
					day = 30;
					break;
				case 10:
					day = 31;
					break;
				case 11:
					day = 30;
					break;
				case 12:
					day = 31;
					break;
				default:
					Console.WriteLine("Enter Month 1-12");
					break;
			}

			return day;
		}
		public static void Alphabet_N_Pattern(int N)
		{

			int index, side_index;
			int Right = 1, Left = 1, Diagonal = 2;
			for (index = 0; index < N; index++)
			{ 
				Console.Write("*");
				for (side_index = 0; side_index < 2 * (index); side_index++)
					Console.Write(" ");
				if (index != 0 && index != N - 1)
					Console.Write("*");
				else
					Console.Write(" ");
				for (side_index = 0;side_index < 2 * (N - index - 1);side_index++)
					Console.Write(" ");
				Console.Write("*");

				Console.Write("\n");
			}
		}

	}				
}
